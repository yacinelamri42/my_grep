#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <stdbool.h>

int match_char(char c, char* regex, int* increment) {
    if (*regex == 0) {
        return -3;
    }
    if (regex[0] == '\\') {
        if (regex[1] == 'd') {
            if (c >= '0' && c <= '9') {
                return 2;
            }
        }else if (regex[1] == 'w') {
            if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z' || (c <= '9' && c >= '0'))) {
                return 2;
            }
        }
    }else if (regex[0] == '(') {
        int j = 0;
        if (regex[j+1] == c) {
            return j+2;
        }
        for (; regex[j] != '|'; j++) {
            if (regex[j] == ')') {
                // something else
            }
            if (regex[j] == 0) {
                return -2;
            }
        }
        if (regex[j+1] == c) {
            return j+2;
        }
    }else if (regex[0] == '|') {
        int j = 0;
        for (; regex[j] != ')'; j++) {
            if (!regex[j]) {
                return -2;
            }
        }
        *increment = 0;
        return j;
    }else if (regex[0] == '[') {
        int j = 0;
        bool positive = true;
        bool found = false;
        if (regex[1] == '^') {
            positive = false;
            j++;
        }
        for (; regex[j] != ']'; j++) {
            if (regex[j] == 0) {
                printf("error\n");
                return -2;
            }
            if (c == regex[j]) {
                found = true;
            }
        }
        if ((positive && found) || (!positive && !found)) {
            return j+1;
        }else {
            return -1;
        }
    }else if (regex[0] == '+') {
        if (regex[-1] == c) {
            return 0;
        }else {
            *increment = 0;
            return 1;
        }
    }else if (regex[0] == ')'){
        *increment = 0;
        return 1;
    }else if (regex[0] == '.') {
        return 1;
    }else {
        if (regex[1] == '?') {
            if (regex[0] == c) {
                return 2;
            }
            *increment = 0;
            return 2;
        }
        if (regex[0] == c) {
            return 1;
        }
    }
    return -1;
}

bool match(char* str, size_t len, char* regex) {
    int str_index = 0;
    char* current_regex_pos = regex;
    bool start_match = false;
    bool in_plus = false;
    if (regex[0] == '^') {
        current_regex_pos++;
        start_match = true;
    }
    int increment = 1;
    for (int i=0; i<=len; i+=increment) {
        increment = 1;
        int pos = match_char(str[i], current_regex_pos, &increment);
        printf("-------------------------------\n");
        printf("c: %c, pos = %d, reg: %c, i: %d, len: %ld\n", str[i], pos, *current_regex_pos, i, len);
        if (pos == -2) {
            assert("pos == -2" && 0);
        }else if (pos == -1) {
            if (start_match) {
                return false;
            }
            current_regex_pos = regex;
        }else if (pos == -3) {
            return true;
        }else {
            // if (!pos) {
            //     in_plus = true;
            // }
            // if (pos == -4) {
            //     in_plus = false;
            //     i--;
            //     pos = 1;
            // }
            // if (pos == -5) {
            //     i--;
            //     pos = 2;
            // }
            // if (pos == -6) {
            //     printf("arrived?\n");
            //     i--;
            //     pos = 1;
            // }
            current_regex_pos+=pos;
            if (*current_regex_pos == '$') {
                if (str[i+1]) {
                    return false;
                }
                current_regex_pos++;
            }
        }
        // printf("c: %c, pos = %d, reg: %c\n", str[i], pos, *current_regex_pos);
    }
    return false;
}

int main(int argc, char* argv[]) {
    // You can use print statements as follows for debugging, they'll be visible when running tests.
    printf("Logs from your program will appear here\n");
    if (argc != 3) {
        fprintf(stderr, "Expected two arguments\n");
        return 1;
    }
    if (argv[1][0] != '-' || argv[1][1] != 'E' || argv[1][2] != 0) {
        fprintf(stderr, "you need -E\n");
        return 1;
    }
    char buf[2048];
    int num_read = 0;
    while ((num_read = read(STDIN_FILENO, buf, 2048)) > 0) {
        if(match(buf, num_read, argv[2])) {
            return 0;
        }
    }
    return 1;
}
