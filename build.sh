#!/bin/bash 

CC=gcc
CFLAGS="-Wall -fsanitize=address -g"
LIBS="-lc"
PROJECT_ROOT=$(dirname $0)
APPNAME="grep"
[ -d $PROJECT_ROOT/bin ] || mkdir $PROJECT_ROOT/bin
$CC $CFLAGS -o $PROJECT_ROOT/bin/$APPNAME $PROJECT_ROOT/src/*.c $LIBS
